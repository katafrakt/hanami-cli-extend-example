require 'hanami/cli/commands'

module Hanami
  module HelloWorld
    class Generate < Hanami::CLI::Command
      desc "prints hello world"
      def call(*)
        puts "Hello World!"
      end
    end
  end
end

Hanami::CLI.register "generate hello_world", Hanami::HelloWorld::Generate